/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlyhosonhansu;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 *
 * @author manhg
 */
public class FXMLDocumentController implements Initializable {
    
    
    @FXML
    private TextField txtAccount;

    @FXML
    private Button btnLogin;

    @FXML
    private PasswordField txtPassword;

    @FXML
    void checkLogin(ActionEvent event) {
        String acc = txtAccount.getText();
        Alert al = new Alert(Alert.AlertType.INFORMATION);
        al.setContentText("Tài khoản của bạn là: "+ acc);
        al.show();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }
    
//    @FXML
//    public void checkLogin(ActionEvent event){
//        
//    }
}
