/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quanlyhosonhansu;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author manhg
 */
public class QuanLyHoSoNhanSu extends Application {
    
    @Override
    public void start(Stage pryStage){
        try{
            Parent root = FXMLLoader.load(this.getClass().getResource("FXMLDocument.fxml"));
            Scene scene = new Scene(root);
            pryStage.setScene(scene);
            pryStage.show();
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
